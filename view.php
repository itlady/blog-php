<?php 
    include 'header.php';
?>

<?php foreach($query as $q) { ?>
    <section class="d-flex flex-column align-items-sm-center">
        <div class="col-sm-8 col-lg-6 my-3 rounded bg-text">
            <h2 class="p-3 bg-success rounded"><a href='view.php?id="<?php echo $q['id'] ; ?>"'><?php echo $q['nazev']; ?></a></h2>
            <div class="px-5">
                <div class="py-3"> <?php echo $q['text']; ?></div>
                <div class="py-2 my-1"> Vloženo: <?php echo $q['datum']; ?></div>
                <div class="py-2 mb-3"> Autor: <?php echo $q['autor']; ?></div>
            </div>
        </div>
        <div class="d-flex flex-row">
            <a href="index.php" class="btn mybtn1 bg-success mx-3" role="button">Zpět</a>
            <a href="edit.php?id=<?php echo $q['id'] ; ?>" name="id" value="<?php echo $q['id'];?>" class="btn mybtn1 bg-warning mx-3" role="button">Upravit článek</a>
        </div>
    </section>    
<?php } ?>




              


