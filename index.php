<?php 
    include 'header.php';
    include 'footer.php';
?>

<main class="container-sm">

    <section class="d-flex flex-column col-sm-10 mx-5 ">
        <?php foreach($query as $q) { ?>
            <div class="d-flex flex-row align-items-center">
                <h2 class="article col-12 mb-3 p-4 bg-success rounded"><a href="view.php?id=<?php echo $q['id'] ; ?>"><?php echo $q['nazev']; ?></a></h2>
                <form method="post" class="delBtn">
                    <input hidden name="nid" value="<?php echo $q['id'];?>">
                    <input name="nsmazat" class="cross" type="submit" value="&times;" title="Smazat článek">
                </form>
            </div>
     
        <?php } ?>
        <a href="add.php" class="mt-3 py-3 btn mybtn2 bg-warning" role="button">Nový článek</a>
    </section>    
    
</main>
    


