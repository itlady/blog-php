<?php 
    include 'knihovna.php';
    include 'config.php';
?>

<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  

    <style>
        :root {
            --text-color: rgb(0,0,0,0.7);
            --text-color-hover: black;
        }
        a { text-decoration: none; color: var(--text-color)}
        a:hover {color: var(--text-color-hover);}
        .bg-text { background: lightgreen;}
        .mybtn1 { width: 150px; }
        .mybtn2 { width: 250px; color: var(--text-color); font-size: 1.75rem;}
        .mybtn2:hover{ font-weight: bolder; background: orangered !important;}
        .article {padding-right: 30px !important;}
        .delBtn {text-indent: -60px;}
        .delBtn:hover path {fill: indianred; }
        .cross {border: none; background: transparent; font-weight: bold; font-size: 3rem; color: gray;}
        .cross:hover {color: lightcoral;}
      
        </style>
</head>
<body>
        <h1 class="text-center mb-5 py-5 alert alert-primary" role="alert"> <?php echo $title ;?> </h1>
        