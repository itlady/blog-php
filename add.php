<?php 

include 'header.php';

?>

<main class="container-sm">
    <div class="d-flex flex-column align-items-sm-center mx-5 p-5 bg-dark rounded-3">
        <form method="post" class="d-flex flex-column col-sm-10 col-lg-6 mt-3">
            <input name="nnazev" placeholder="Nadpis článku" class="rounded py-2">
            <textarea name="ntext" cols="30" rows="10" placeholder="Text článku" class="my-2"></textarea>
            <input name="nautor" placeholder="Autor článku" class="rounded py-2">
            <div class="d-flex flex-row justify-content-around">
                <a href="index.php"><input type="button" class="py-2 px-5 mt-4 bg-warning rounded" value="Zrušit"></a>
                <input type="submit" name="nnovy" class="py-2 px-5 mt-4 bg-info rounded" value="Uložit">
            </div>
        </form>
    </div>
    
    
    
</main>


