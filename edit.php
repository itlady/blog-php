<?php 

include 'header.php';

?>

<?php foreach($query as $q) {?>
    <div class="d-flex flex-column align-items-sm-center mx-5 p-5 bg-dark rounded-3">
        <form method="post" class="d-flex flex-column col-sm-10 col-lg-6 mt-3">
            <input hidden name="nid" value="<?php echo $q['id'];?>">
            <input name="nnazev" class="rounded py-2" value="<?php echo $q['nazev'];?>">
            <textarea name="ntext" cols="30" rows="10" class="my-2"><?php echo $q['text'];?></textarea>
            <input name="nautor" class="rounded py-2" value="<?php echo $q['autor'];?>">
            <div class="d-flex flex-row justify-content-around">
                <a href="index.php"><input type="button" class="py-2 px-5 mt-4 bg-warning rounded" value="Zrušit"></a>
                <input type="submit" name="nupdate" class="py-2 px-5 mt-4 bg-info rounded" value="Uložit">
            </div>
        </form>
    </div>
    





<?php }?>